FROM node:18.19.0-alpine3.19

# Create app directory
WORKDIR /dist
RUN apk update && \
    apk add --no-cache --update -v git
# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
#COPY package*.json ./
COPY . .
# RUN mv ./app/cert_and_key ./app/src/cert_and_key
RUN npm install
# If you are building your code for production
# RUN npm ci --only=production
# API https
EXPOSE 9999 \
#API http
       9998 \
       9803

CMD [ "npm", "run-script", "start" ]
